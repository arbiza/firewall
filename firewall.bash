#!/bin/bash

## Copyright (c) 2016 Lucas Arbiza
##
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
## General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with this program.  If not, see http://www.gnu.org/licenses/.
##
## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##
## DESCRIPTION
##
## This script deploys a dual-stack (v6 and v4) statefull firewall implemented
## using Netfilter iptables. It has been designed to be used in servers.
## This script closes everything and open only what is needed. In order to
## enable the proper IPv6 communication it follows the RFC 4890 recomendations.
##
##
## IPv6 settings based on:
## - https://www.cert.org/downloads/IPv6/ip6tables_rules.txt
## - https://www.ietf.org/rfc/rfc4890.txt
## - Apostille for laboratories itinerary of the Nic.br's IPv6 course
##
## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##
## USAGE:
##
## Server side:
##
##   1) Edit the variables OPEN_TCP_PORTS and OPEN_UDP_PORTS just bellow
##      according your needs;
##
##   2) You may fix the ports to be knocked in this file instead of passing the
##      ports as arguments setting port_knocked_X variables.
##
##   3) If you prefer to use another port than 22 (default) for SSH, you must
##      change it in the Port Knocking section.
##
##   4) In the function store_rules (almost at the end of this file) uncomment
##      or add the rules to store the ruleset according the operating system
##      you are using.
##
##   5) Run:
##          /bin/bash firewall.bash start <port 1> <port 2> <port 3>
##
##              OR, if you defined the ports editing this script:
##
##          /bin/bash firewall.bash start
##
##
## Client side:
##
##   HOST='your server address or name'
##   for porta in <port 1> <port 2> <port 3>
##   do
##      nmap -Pn --host_timeout 201 --max-retries 0 -p $porta $HOST
##   done
##   ssh $HOST
##
## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##
## EDIT HERE!
##
## Add or remove the services ports as you need. You may use service name
## or port number. Keep an space between ports. Firewall allows OUTPUT to
## related and established connections, if you need to open a port for OUTPUT
## (e.g.: SMTP) list the port in OPEN_TCP_PORTS_OUT_ONLY -- DNS, HTTP(s) ans
## SSH port are already open for OUTPUT.
##
## Example:
##     OPEN_TCP_PORTS='http https smpt smpts imap imaps'
##   Using sed:
##     sed -i "s/OPEN_TCP_PORTS=''/OPEN_TCP_PORTS='http http'/" firewall.bash
##
## DO NOT include SSH port! It's already covered by port knocking.

OPEN_TCP_PORTS=''
OPEN_TCP_PORTS_OUT_ONLY=''
OPEN_UDP_PORTS=''
OPEN_UDP_PORTS_OUT_ONLY=''

## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##
##
## EDIT HERE! if you want
## You may define ports to be knocked here replacing '$' by the port's number
## you want to use; or you may define passing port as arguments when you run
## this script.
##
## Example:
##   port_knocked_1=1111
##   port_knocked_2=2222
##   port_knocked_3=3333
## Using sed:
##   sed -i "s/port_knocked_1=\$2/port_knocked_1=1111/" firewall.bash
##   sed -i "s/port_knocked_1=\$3/port_knocked_2=2222/" firewall.bash
##   sed -i "s/port_knocked_1=\$4/port_knocked_3=3333/" firewall.bash


port_knocked_1=$2
port_knocked_2=$3
port_knocked_3=$4

## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -



PATH=/sbin:/bin:/usr/sbin:/usr/bin

ip4='/sbin/iptables'
ip6='/sbin/ip6tables'




start_firewall () {

    # Opening firewall and removing existing rules
    stop_firewall


    for ipX in $ip6 $ip4
    do

        # Keep established connections up
        $ipX -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
        $ipX -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

        # Set policy to drop
        $ipX -P INPUT DROP
        $ipX -P OUTPUT DROP
        $ipX -P FORWARD DROP

        ## Communication that server is allowed to start

        # Accept everything from loopback
        $ipX -A INPUT -i lo -j ACCEPT
        $ipX -A OUTPUT -o lo -j ACCEPT

        # Allow HTTP and HTTPS output to enable server updates
        $ipX -A OUTPUT -p tcp --dport http -j ACCEPT
        $ipX -A OUTPUT -p tcp --dport https -j ACCEPT

        # Server may start SSH connections
        $ipX -A OUTPUT -p tcp --dport ssh -j ACCEPT

        # DNS
        $ipX -A OUTPUT -p udp --sport 1024:65535 --dport 53 -j ACCEPT
        $ipX -A OUTPUT -p tcp --sport 1024:65535 --dport 53 -j ACCEPT


        # SERVICES

        # TCP services ports to be open
        for service_tcp_port in $OPEN_TCP_PORTS
        do
            $ipX -A INPUT -p tcp --dport $service_tcp_port -j ACCEPT
        done

        for service_tcp_port in $OPEN_TCP_PORTS_OUT_ONLY
        do
            $ipX -A OUTPUT -p tcp --sport $service_tcp_port -j ACCEPT
        done

        # UDP services ports to be open
        for service_udp_port in $OPEN_UDP_PORTS
        do
            $ipX -A INPUT -p udp --dport $service_udp_port -j ACCEPT
        done

        for service_udp_port in $OPEN_UDP_PORTS_OUT_ONLY
        do
            $ipX -A OUTPUT -p udp --sport $service_udp_port -j ACCEPT
        done

    done



    ## IPv6 and ICMPv6
    ## ________________________________________________________________________
    ##
    ## ICMP has different rules for IPv6 and IPv4
    ##

    # Disable RH0
    $ip6 -A INPUT -m rt --rt-type 0 -j DROP

    # Discard documentation addresses
    $ip6 -A INPUT -s 2001:0db8::/32 -j DROP

    # Allow traceroute
    $ip6 -A INPUT -p udp -m udp --dport 33434:33523 -m state --state NEW -j REJECT --reject-with icmp6-port-unreachable



    ## ICMPv6
    ##
    ## Types addressed (RFC 4890):
    ## - 1	Destination Unreachable
    ## - 2	Packet Too Big
    ## - 3	Time Exceeded
    ## - 4	Parameter Problem
    ## - 128	Echo Request
    ## - 129	Echo Reply
    ## - 130	Multicast Listener Query
    ## - 131	Multicast Listener Report
    ## - 132	Multicast Listener Done
    ## - 133	Router Solicitation
    ## - 134	Router Advertisement
    ## - 135	Neighbor Solicitation
    ## - 136	Neighbor Advertisement
    ## - 137	Redirect Message
    ## - 141	Inverse Neighbor Discovery
    ## - 142	Inverse Neighbor Discovery
    ## - 143	Listener Report v2
    ## - 148	Certificate Path Solicitation
    ## - 149	Certificate Path Advertisement
    ## - 151	Multicast Router Advertisement
    ## - 152	Multicast Router Solicitation
    ## - 153	Multicast Router Termination

    #  The following rules are applied to the INPUT and OUTPUT chains.
    for chain in INPUT OUTPUT
    do
        # Types 1, 2, 3, 4
        $ip6 -A $chain -p icmpv6 --icmpv6-type destination-unreachable -j ACCEPT
        $ip6 -A $chain -p icmpv6 --icmpv6-type packet-too-big -j ACCEPT
        $ip6 -A $chain -p icmpv6 --icmpv6-type time-exceeded -j ACCEPT
        $ip6 -A $chain -p icmpv6 --icmpv6-type parameter-problem -j ACCEPT

        # Echo request and echo reply
        # types 128, 129
        $ip6 -A $chain -p icmpv6 --icmpv6-type echo-request -m limit --limit 5/sec --limit-burst 10 -j ACCEPT
        $ip6 -A $chain -p icmpv6 --icmpv6-type echo-reply -m limit --limit 5/sec --limit-burst 10 -j ACCEPT

        # Multicast -- only link local
        # types 130, 131, 132
        $ip6 -A $chain -s fe80::/10 -p icmpv6 --icmpv6-type 130 -j ACCEPT
        $ip6 -A $chain -s fe80::/10 -p icmpv6 --icmpv6-type 131 -j ACCEPT
        $ip6 -A $chain -s fe80::/10 -p icmpv6 --icmpv6-type 132 -j ACCEPT

        # Multicast -- link local only
        # types 133, 134
        $ip6 -A $chain -p icmpv6 --icmpv6-type 133 -j ACCEPT
        $ip6 -A $chain -p icmpv6 --icmpv6-type 134 -j ACCEPT

        # Neighgor Discovery Protocol --- only if the hop limit field is 255
        # types 135, 136
        $ip6 -A $chain -p icmpv6 --icmpv6-type neighbor-solicitation -m hl --hl-eq 255 -j ACCEPT
        $ip6 -A $chain -p icmpv6 --icmpv6-type neighbor-advertisement -m hl --hl-eq 255 -j ACCEPT

        # Inverse neighbor discovery
        # types 141, 142
        $ip6 -A $chain -p icmpv6 --icmpv6-type 141 -j ACCEPT
        $ip6 -A $chain -p icmpv6 --icmpv6-type 142 -j ACCEPT

        # Listener report -- type 143
        $ip6 -A $chain -p icmpv6 --icmpv6-type 143 -j ACCEPT

        # Certificate path
        # types 148, 149
        $ip6 -A $chain -p icmpv6 --icmpv6-type 148 -j ACCEPT
        $ip6 -A $chain -p icmpv6 --icmpv6-type 149 -j ACCEPT

        # Multicast router
        # types 151, 152, 153
        $ip6 -A $chain -p icmpv6 --icmpv6-type 151 -j ACCEPT
        $ip6 -A $chain -p icmpv6 --icmpv6-type 152 -j ACCEPT
        $ip6 -A $chain -p icmpv6 --icmpv6-type 153 -j ACCEPT

    done

    # Redirects are allowed only when comming.
    # type 137
    $ip6 -A INPUT -p icmpv6 --icmpv6-type redirect -m hl --hl-eq 255 -j ACCEPT
    $ip6 -A OUTPUT -p icmpv6 --icmpv6-type redirect -j DROP

    # Log dropped ICMPv6 packets --- they will be dropped due to de default
    # policy
    $ip6 -A INPUT -p icmpv6 -j LOG --log-prefix "dropped ICMPv6"



    # ICMP v4: The following rules allow input and output of icmp packets (type
    # echo request); replys are allowed through the rule that allows packets of
    # connections established and related.
    $ip4 -A INPUT -p icmp --icmp-type 8 -m state --state NEW,ESTABLISHED,RELATED -m limit --limit 5/sec --limit-burst 10  -j ACCEPT
    $ip4 -A OUTPUT -p icmp --icmp-type 8 -m state --state NEW,ESTABLISHED,RELATED -m limit --limit 5/sec --limit-burst 10 -j ACCEPT




    ## PORT KNOCKING
    ## ________________________________________________________________________
    ##
    ## Knocking process is explained in the comments.

    for ipX in $ip6 $ip4
    do
        # Create states chains
        $ipX -N STATE2
        $ipX -N STATE3

        # 3rd: Process packets from the connection marked as 'KNOCK1'; connection is marked as 'KNOCK2'
        $ipX -A STATE2 -m recent --name KNOCK1 --remove
        $ipX -A STATE2 -m recent --name KNOCK2 --set
        $ipX -A STATE2 -j LOG --log-prefix "PORT KNOCKING - STATE 2: "

        # 5th: Process packets from the connection marked as 'KNOCK2'; connection is marked as 'KNOCK3'
        $ipX -A STATE3 -m recent --name KNOCK2 --remove
        $ipX -A STATE3 -m recent --name KNOCK3 --set
        $ipX -A STATE3 -j LOG --log-prefix "PORT KNOCKING - STATE 3: "


        # 1st: When the first port is knocked the connection is marked as 'KNOCK1'
        $ipX -A INPUT -p tcp --dport $port_knocked_1 -m recent --set --name KNOCK1

        # 2nd: Connection marked as 'KNOCK1' knocking the second port is redirected to the 'STATE2' chain.
        $ipX -A INPUT -p tcp --dport $port_knocked_2 -m recent --rcheck --seconds 30 --name KNOCK1 -j STATE2

        # 4th: Connection marked as 'KNOCK2' knocking the third port is redirected to the 'STATE3' chain.
        $ipX -A INPUT -p tcp --dport $port_knocked_3 -m recent --rcheck --seconds 30 --name KNOCK2 -j STATE3

        # 6th and final: SSH connection allowed for 5 minutes to the connection marked as 'KNOCK3'
        $ipX -A INPUT -p tcp --dport ssh -m recent --rcheck --seconds 300 --name KNOCK3 -j ACCEPT

    done



    echo
    echo
    echo
    echo '  FIREWALL [RE]STARTED'
    echo
    echo '  In order to connect to this server by SSH, knock the following'
    echo "  ports before to try to connect to SSH:  $1"
    echo "                                          $2"
    echo "                                          $3"
    echo
    echo "  Run '/bin/bash $0 stop' to stop the firewall and to flush the rules".
    echo
    echo

} ## Close start_firewall




# This function change default policy to ACCEPT and deletes every rule and chains
stop_firewall () {

    for ipX in $ip6 $ip4
    do

        # Input and output policies are changed to accept to keep
        # existing connections while firewall restarts.
        $ipX -P INPUT ACCEPT
        $ipX -P OUTPUT ACCEPT
        $ipX -P FORWARD ACCEPT

        # Flush the ruleset
        $ipX -F

        # Delete all non-default chains
        $ipX -X
    done

} ## Close stop_firewall




wrong_usage () {

    echo
    echo
    echo
    echo "  WRONG USAGE! $1"
    echo
    echo "  Usage options:"
    echo
    echo "    STARTING:"
    echo
    echo "      If you have defined the ports to be knocked in the script:"
    echo "      # /bin/bash $0 start"
    echo
    echo "      If you prefer to define the ports to be knocked when running:"
    echo "      # /bin/bash $0 start 1st_port 2nd_port 3rd_port"
    echo
    echo "    STOPING:"
    echo
    echo "      Stoping the firewall will flush all the rules and set policy to ACCEPT."
    echo "      # /bin/bash $0 stop"
    echo
    echo
    echo

    exit 1

} ## Close wrong_usage



store_rules () {

    ## Uncomment the lines with the commands according to the Linux distro you
    ## are using or add the commands as you need.

    ## Archlinux
    #arch# $ip6-save > /etc/iptables/ip6tables.rules
    #arch# $ip4-save > /etc/iptables/iptables.rules
    ##
    ##
    ## CentOS
    ## Install the package iptables-services and enable it doing:
    ## # systemctl enable iptables
    ## # systemctl enable ip6tables
    #centos# /sbin/service ip6tables save
    #centos# /sbin/service iptables save
    ##
    ##
    ## Ubuntu
    ## Install the package iptables-persistent
    #ubuntu# $ip6-persistent save
    #ubuntu# $ip4-persistent save
    ##
    ##
    ## Debian
    ## Restoring rules on boot on Debian is made trought the following script
    ## you need to manually create with the following content:
    ##
    ## <editor> /etc/network/if-pre-up.d/iptables
    ## #!/bin/sh
    ## /sbin/ip6tables-restore < /etc/ip6tables.up.rules
    ## /sbin/iptables-restore < /etc/iptables.up.rules
    ##
    ## Change file permissions:
    ## chmod +x /etc/network/if-pre-up.d/iptables
    ##
    #debian# $ip6-save > /etc/ip6tables.up.rules
    #debian# $ip4-save > /etc/iptables.up.rules


} ## Close store_rules



## Main

if [ "$1" == 'start' ]; then

    if [ -z $port_knocked_1 ] || [ -z $port_knocked_2 ] || [ -z $port_knocked_3 ]
    then
        wrong_usage '-> Ports to be knocked have not being defined.'
    else
        start_firewall $port_knocked_1 $port_knocked_2 $port_knocked_3
        store_rules
    fi

elif [ "$1" == 'stop' ]; then

    stop_firewall
    store_rules

    echo
    echo
    echo '  FIREWALL STOPPED'
    echo '  Default policy changed to ACCEPT.'
    echo
    echo

else
    wrong_usage
fi
