
#Server Firewall#

This Bash script implements a firewall to be used in servers, including those exposed to the Internet. This firewall is IPv6 ready.

The list bellow describes features implemented:

* Policy employed: Deny all; explicit allow
* In accordance with RFC 4890 recommendations for IPv6
* Port Knocking required to open SSH port
* Server allowed to start SSH, HTTP and HTTPS connections and to make DNS requests (any other functionality must be explicitly allowed)
* Easy to use


##Usage Description##

###Server side###

Clone and enter into the cloned directory:

```bash
git clone https://arbiza@bitbucket.org/arbiza/firewall.git
cd firewall
```

Open the file _firewall.bash_ for editing:

1. Add or remove TCP and UDP ports to be open editing the variables _OPEN\_TCP\_PORTS_, _OPEN\_TCP\_PORTS\_OUT\_ONLY_, _OPEN\_UDP\_PORTS_, and _OPEN\_UDP\_PORTS\_OUT\_ONLY_.
2. Define the ports to be used in Port Knocking editing the three variables named _port\_knocked\_<number>_. If you prefer you may also to inform these ports by arguments when you run this script
3. If you prefer to use another port than default for SSH, you must change it in last line of the Port Knocking section.
4. In the function store_rules (almost at the end of this file) uncomment or add the rules to store the ruleset if you wish.

Run the script using one of the two forms bellow:

If you defined the ports to be used in Port Knocking inside the file, just run:

```bash
/bin/bash firewall.bash start
```

If you wish to inform the ports to be used in Port Knocking by arguments, run:

```bash
/bin/bash firewall.bash start <port 1> <port 2> <port 3>
```


###Client side###

One way you may connect to the server is by using Nmap to "_knock the doors_". This repository contains a script that _knocks_ and open a SSH connection.

Edit the script _knock-and-connect.bash_ as instruct in its header and run doing:

```bash
/bin/bash knock-and-connect.bash
```
