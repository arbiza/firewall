#!/bin/bash

##
## EDIT THE FOLLOWING VARIABLES AS YOU NEED:
##
## IP address version.
## Both Nmap and SSH must communicate using the same stack, otherwise port 
## knocking won't open SSH port. Options are 6 or 4 (number only).
ip_version='6'

## Edit if you use a different port for SSH than default
ssh_port='22'

## Set your user
user=''

## Set target host
host=''

## Set the thre ports to be knocked (space separated)
## Example: ports='1111 2222 3333'
ports=''



## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -



echo
echo "  Knocking on server $host"
echo "  IP stack: v$ip_version"
echo


for port in $ports
do
    echo "    Knocking port $port"

    if  [ "$ip_version" == '6' ]; then
        nmap -6 -Pn --host_timeout 201 --max-retries 2 -p $port $host 1> /dev/null
    else
        nmap -Pn --host_timeout 201 --max-retries 0 -p $port $host 1> /dev/null
    fi
done

echo 

ssh -$ip_version $user@$host
